import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 13.04.2017.
 */
public class baseClass {

    public WebDriver driver;

    //Step One locators

    By personalAcc = By.cssSelector(".button.js-mfp-ajax.login__icon.button--popup.button--def-only-icon");
    By email = By.cssSelector("#email1");
    By password = By.cssSelector("#password1");
    By login = By.cssSelector(".button.js-form-submit.button--authorization");

    //Verify login locators

    By textOnServMessage = By.cssSelector(".noty_text");

    //Step two locators

    By catalogMenu = By.cssSelector(".button.button--expand-text.button--catalogue.js-dropdown-toggle");
    By categoryList = By.xpath("html/body/div[1]/header/div/div[2]/div/div[1]/div/div/ul/li[2]/a");
    By category = By.xpath("html/body/div[1]/div[1]/div/div/div[2]/div/div/div[3]/div/a[1]");
    By goodsItem = By.xpath("html/body/div[1]/div[1]/div/div/div[2]/div/div/div[3]/div/a");
    By clickBuy = By.xpath(".//*[@id='list-toggle']/div[1]/div/div/div[1]/div[3]/div[3]/div[2]/div/button");
    By createOrder = By.cssSelector(".button.button--order");

    //Step Three locators

    By delivery = By.xpath("html/body/div[1]/div[1]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/span/label[3]/ins");
    By openCitiesList = By.xpath(".//*[@id='select2-application-select2-container']");
    By chooseCity = By.xpath("html/body/span/span/span[2]/ul/li[5]");
    By address = By.cssSelector("#address3");
    By payment = By.xpath("html/body/div[1]/div[1]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div[12]/div/label[2]/ins");
    By comment = By.cssSelector("#textarea3");
    By order = By.cssSelector(".button.js-form-submit.button--order");

    //Verify order locators

    By textOnMessage = By.cssSelector(".noty_text");

}
