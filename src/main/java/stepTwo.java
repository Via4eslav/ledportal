import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 12.04.2017.
 */
public class stepTwo extends baseClass {

    public stepTwo(WebDriver driver){this.driver = driver;}

    public void setCatalogMenu(){driver.findElement(catalogMenu).click();}

    public void setCategoryList(){driver.findElement(categoryList).click();}

    public void setCategory(){driver.findElement(category).click();}

    public void setGoodsItem(){driver.findElement(goodsItem).click();}

    public void setClickBuy(){driver.findElement(clickBuy).click();}

    public void setCreateOrder(){driver.findElement(createOrder).click();}

    public void choosingThis(){

        this.setCatalogMenu();
        this.setCategoryList();
        this.setCategory();
        this.setGoodsItem();
        this.setClickBuy();
        this.setCreateOrder();
    }
}
