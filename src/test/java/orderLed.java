import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by ivanov.v on 12.04.2017.
 */
public class orderLed {
    WebDriver driver;
    stepOne objStepOne;
    verifyLogin objBerifyLogin;
    stepTwo objStepTwo;
    stepThree objStepThree;
    verifyOrder objVerifyOrder;

    @BeforeTest

    public void setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://1:1@ledportal.wezom.net");
    }

    @Test

    public void makingOrder() throws InterruptedException {
        objStepOne = new stepOne(driver);
        objStepOne.authorization("automat@automat.au", "000000");

        objBerifyLogin = new verifyLogin(driver);
        Assert.assertTrue(objBerifyLogin.getTextOnServMessage().contains("Вы успешно авторизовались на сайте!"));

        objStepTwo = new stepTwo(driver);
        objStepTwo.choosingThis();

        objStepThree = new stepThree(driver);
        objStepThree.enterDataStepThree("Domodedovo","Lorem ipsum, gospoda!");

        objVerifyOrder = new verifyOrder(driver);
        Assert.assertTrue(objVerifyOrder.getTextOnVerifyMessage().contains("Вы успешно оформили заказ! Спасибо за то что вы с нами"));
    }

}
