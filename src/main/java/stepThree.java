import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 12.04.2017.
 */
public class stepThree extends baseClass {

    public stepThree(WebDriver driver){this.driver = driver;}

    public void setDelivery(){driver.findElement(delivery).click();}

    public void setOpenCitiesList(){driver.findElement(openCitiesList).click();}

    public void setChooseCity(){driver.findElement(chooseCity).click();}

    public void setAddress(String strAddress){driver.findElement(address).sendKeys(strAddress);}

    public void setPayment(){driver.findElement(payment).click();}

  //  public void setComment(String strComment){driver.findElement(comment).sendKeys(strComment);}

    public void setOrder(){driver.findElement(order).click();}

    public void enterDataStepThree(String strAddress, String strComment) throws InterruptedException {
        this.setDelivery();
        this.setOpenCitiesList();
        this.setChooseCity();
        this.setAddress(strAddress);
        Thread.sleep(2000);
        this.setPayment();
       // this.setComment(strComment);
        this.setOrder();
    }


}
