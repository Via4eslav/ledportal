import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 12.04.2017.
 */
public class stepOne extends baseClass {

    public stepOne(WebDriver driver){this.driver = driver;}

    public void setPersonalAcc(){driver.findElement(personalAcc).click();}

    public void setEmail(String strEmail){driver.findElement(email).sendKeys(strEmail);}

    public void setPassword(String strPassword){driver.findElement(password).sendKeys(strPassword);}

    public void setLogin(){driver.findElement(login).click();}

    public void authorization(String strEmail, String strPassword){
        this.setPersonalAcc();
        this.setEmail(strEmail);
        this.setPassword(strPassword);
        this.setLogin();
    }



}
